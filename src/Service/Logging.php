<?php
/** *****************************************************************************************************************
 *  Logging.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/25
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\Service;

use Flagstone\ActionLoggingBundle\Entity\Log;
use Flagstone\ActionLoggingBundle\Entity\LogEntityInterface;
use Flagstone\ActionLoggingBundle\Log\LogInterface;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;

/** *****************************************************************************************************************
 *  Class Logging
 *  -----------------------------------------------------------------------------------------------------------------
 *  Log data
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\ActionLoggingBundle
 *  ***************************************************************************************************************** */
class Logging
{
    /**
     *  @var LogInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private LogInterface    $logResource;
    private Log             $log;

    /** *************************************************************************************************************
     *  Logging constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LogInterface $logResource
     *  ************************************************************************************************************* */
    public function __construct(LogInterface $logResource, RequestStack $requestStack)
    {
        $this->logResource = $logResource;
        $this->log = new Log();
        if (null !== $requestStack->getCurrentRequest()) {
            $this->log->setIp($requestStack->getCurrentRequest()->getClientIp());
        }
    }

    /**
     *  Save the log
     *  NO_LOG log level allow to avoid logging (for example in Listener calls with command or admin interface)
     *
     *  @param int         $level
     *  @param string      $action
     *  @param string      $context
     *  @param int|null    $user
     *  @param int|null    $concern
     *  @param string|null $message
     *
     *  @return void
     */
    public function log(int $level, string $action, string $context, ?int $user, ?int $concern, ?string $message): void
    {
        if (LogEntityInterface::LOG_LEVEL['NO_LOG'] !== $level) {
            $this->log
                ->setConcern($concern)
                ->setUser($user)
                ->setContext($context)
                ->setAction($action)
                ->setLevel($level)
                ->setMessage($message)
                ->setCreatedAt(new DateTime());
            $this->logResource->save($this->log);
        }
    }

    /**
     *  Alias to save Error log
     *
     * @param string      $action
     * @param string      $context
     * @param int|null    $user
     * @param int|null    $concern
     * @param string|null $message
     *
     * @return void
     * @throws Exception
     */
    public function error(string $action, string $context, ?int $user = null, ?int $concern = null, ?string $message = null): void
    {
        $this->log(LogEntityInterface::LOG_LEVEL['ERROR'], $action, $context, $user, $concern, $message);
    }

    /**
     *  Alias to save Info log
     *
     * @param string      $action
     * @param string      $context
     * @param int|null    $user
     * @param int|null    $concern
     * @param string|null $message
     *
     * @return void
     * @throws Exception
     */
    public function info(string $action, string $context, ?int $user = null, ?int $concern = null, ?string $message = null): void
    {
        $this->log(LogEntityInterface::LOG_LEVEL['INFO'], $action, $context, $user, $concern, $message);
    }

    /**
     *  Alias to save Info log
     *
     * @param string      $action
     * @param string      $context
     * @param int|null    $user
     * @param int|null    $concern
     * @param string|null $message
     *
     * @return void
     * @throws Exception
     */
    public function dump(string $action, string $context, ?int $user = null, ?int $concern = null, ?string $message = null): void
    {
        $this->log(LogEntityInterface::LOG_LEVEL['DUMP'], $action, $context, $user, $concern, $message);
    }
}