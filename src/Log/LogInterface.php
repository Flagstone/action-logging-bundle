<?php
/** *****************************************************************************************************************
 *  LogInterface.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/22
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\Log;

use Flagstone\ActionLoggingBundle\Entity\LogEntityInterface;

interface LogInterface
{
    public function save(LogEntityInterface $log);
}