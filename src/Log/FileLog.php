<?php
/** *****************************************************************************************************************
 *  DatabaseLog.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/22
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\Log;

use Flagstone\ActionLoggingBundle\Entity\LogEntityInterface;
use Flagstone\ActionLoggingBundle\Formatter\LogFormatter;
use Exception;

/** *****************************************************************************************************************
 *  Class FileLog
 *  -----------------------------------------------------------------------------------------------------------------
 *  Save log in file
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\ActionLoggingBundle
 *  ***************************************************************************************************************** */
class FileLog implements LogInterface
{
    /**
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    private string $filename;

    /**
     *  @var LogFormatter
     *  ------------------------------------------------------------------------------------------------------------- */
    private LogFormatter $formatter;

    /** *************************************************************************************************************
     *  FileLog constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LogFormatter $formatter
     *  @param string $filename
     *  ************************************************************************************************************* */
    public function __construct(LogFormatter $formatter, string $filename)
    {
        $this->filename = $filename;
        $this->formatter = $formatter;
    }

    /** *************************************************************************************************************
     *  Save the log in a file, in app var/log directory (writable by default)
     *  If the file doesn't exist, it will be created
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LogEntityInterface $log
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function save(LogEntityInterface $log): void
    {
        $stringLog = $this->formatter->build($log);
        file_put_contents($this->filename, $stringLog.PHP_EOL, FILE_APPEND);
    }
}