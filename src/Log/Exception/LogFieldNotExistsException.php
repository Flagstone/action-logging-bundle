<?php
/** *****************************************************************************************************************
 *  LogFieldNotExistsException.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2019/11/13
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\Log\Exception;

use ReflectionException;

class LogFieldNotExistsException extends ReflectionException
{

}