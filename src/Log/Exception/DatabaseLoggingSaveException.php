<?php
/** *****************************************************************************************************************
 *  DatabaseLoggingSaveException.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2022/01/25
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\Log\Exception;

use Exception;

class DatabaseLoggingSaveException extends Exception
{

}