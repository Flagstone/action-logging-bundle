<?php
/** *****************************************************************************************************************
 *  LogFieldTypeErrorException.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2022/01/24
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\Log\Exception;

use Exception;

class LogFieldTypeErrorException extends Exception
{

}