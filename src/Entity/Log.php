<?php

namespace Flagstone\ActionLoggingBundle\Entity;

use Flagstone\BaseEntityBundle\BaseEntity\AbstractMappedSuperclass;
use Doctrine\ORM\Mapping as ORM;
use Flagstone\ActionLoggingBundle\Annotation\LogLevel;

/**
 *  Class Log
 *
 *  @package Flagstone\ActionLoggingBindle\Entity
 */
#[ORM\Entity]
#[ORM\Table(name: 'action_logging')]
class Log extends AbstractMappedSuperclass implements LogEntityInterface
{
    /**
     *  @var null|int The id of the user that make the action. Can be null if user is 'app'
     */
    #[ORM\Column(name: 'user_id', type: 'integer', nullable: true )]
    private ?int $user = null;

    /**
     *  @var string The action made
     */
    #[ORM\Column(name: 'action', type:'string', length: 100, nullable: false )]
    private string $action;

    /**
     *  @var string The context of the action, btw the class or the bundle that that make the action
     */
    #[ORM\Column(name: 'context', type: 'string', length: 50, nullable: false)]
    private string $context;

    #[ORM\Column(name: 'ip', type: 'string', length: 50, nullable: true)]
    private ?string $ip;

    /**
     *  @var null|int The id of the user concerned by the action. Can be null if the actions doesn't concern a user
     */
    #[ORM\Column(name: 'concern', type: 'integer', nullable: true)]
    private ?int $concern = null;

    /**
     *  @var int The level of the log
     */
    #[ORM\Column(name: 'level', type: 'integer', nullable: false)]
    private int $level;

    /**
     *  @var string The message associated to the action (error message for example)
     */
    #[ORM\Column(name: 'message', type: 'string', length: 1024, nullable: true)]
    private ?string $message;

    /**
     * @param int|null $user
     *
     * @return $this
     */
    public function setUser(?int $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param string $action
     *
     * @return $this
     */
    public function setAction(string $action): self
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @param string|null $context
     *
     * @return $this
     */
    public function setContext(?string $context): self
    {
        $this->context = $context;
        return $this;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @param int|null $concern
     *
     * @return $this
     */
    public function setConcern(?int $concern): self
    {
        $this->concern = $concern;
        return $this;
    }

    /**
     * @param int $level
     *
     * @return $this
     */
    public function setLevel(int $level): self
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @param string|null $message
     *
     * @return $this
     */
    public function setMessage(?string $message): self
    {
        $this->message = strip_tags($message);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return string|null
     */
    public function getContext(): ?string
    {
        return $this->context;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @return int|null
     */
    public function getConcern(): ?int
    {
        return $this->concern;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}
