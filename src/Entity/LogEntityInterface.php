<?php

namespace Flagstone\ActionLoggingBundle\Entity;

use Flagstone\BaseEntityBundle\BaseEntity\BaseEntityInterface;
use Flagstone\BaseEntityBundle\BaseEntity\SuperclassInterface;

interface LogEntityInterface extends BaseEntityInterface, SuperclassInterface
{
    /*  =============================================================================================================
     *  Constants
     *  ============================================================================================================= */
    const LOG_LEVEL = [
        'NO_LOG' => 0,
        'DEBUG' => 1,
        'INFO' => 2,
        'WARNING' => 3,
        'ERROR' => 4,
        'CRITICAL' => 5
    ];
}