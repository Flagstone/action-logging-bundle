<?php
/** *****************************************************************************************************************
 *  FlagstoneActionLoggingBundle.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/24
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/** *****************************************************************************************************************
 *  Class FlagstoneActionLoggingBundle
 *  -----------------------------------------------------------------------------------------------------------------
 *  This bundle provides tools to log all action (define by the programmer) on a bundle or an App.
 *  It uses a log file or a table (fv_action_log by default) in the database.
 *  A user interface is also develop to access easily to the actions log.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\ActionLoggingBundle
 *  ***************************************************************************************************************** */
class FlagstoneActionLoggingBundle extends Bundle
{

}