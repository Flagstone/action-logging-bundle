<?php
/** *****************************************************************************************************************
 *  LogFormatter.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2019 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2022/01/24
 *  ***************************************************************************************************************** */
namespace Flagstone\ActionLoggingBundle\Formatter;

use Flagstone\ActionLoggingBundle\Entity\LogEntityInterface;
use Exception;
use Flagstone\ActionLoggingBundle\Log\Exception\LogFieldNotExistsException;
use Flagstone\ActionLoggingBundle\Log\Exception\LogFieldTypeErrorException;
use TypeError;
use ReflectionException;

/** *****************************************************************************************************************
 *  Class LogFormatter
 *  -----------------------------------------------------------------------------------------------------------------
 *  Format the log for the file log feature.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\ActionLoggingBundle\Formatter
 *  ***************************************************************************************************************** */
class LogFormatter
{
    /**
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    private $logFormat;

    /** *************************************************************************************************************
     *  LogFormatter constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $logFormat
     *  ************************************************************************************************************* */
    public function __construct(string $logFormat)
    {
        $this->logFormat = $logFormat;
    }

    /** *************************************************************************************************************
     *  @return string
     *  ************************************************************************************************************* */
    public function getFormat(): string
    {
        return $this->logFormat;
    }

    /** *************************************************************************************************************
     *  Build and format the log string.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LogEntityInterface $log
     *  @return string
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function build(LogEntityInterface $log): string
    {
        $pattern = "/{([a-zA-Z_]*)}/";
        preg_match_all($pattern, $this->logFormat, $matches);
        $finalString = $this->logFormat;

        $toReplace = $matches[0];
        $fields = $matches[1];

        try {
            foreach ($toReplace as $key => $stringToReplace) {
                $field = preg_replace_callback('/_[a-zA-Z]/', function($matchs) { return strtoupper(substr($matchs[0], 1, 1)); }, $fields[$key]);
                $methodName = 'get'.ucfirst($field);

                $method = new \ReflectionMethod(get_class($log), $methodName);
                $value = $method->invoke($log);

                if ('object' === gettype($value)) {
                    $valueClass = get_class($value);
                    if ($valueClass === 'DateTime') {
                        $value = $value->format('Y/m/d H:i:s');
                    }
                } else {
                    $class = new \ReflectionClass(get_class($log));
                    $fieldName = $class->getProperty($field);
                    $annotations = $fieldName->getDocComment();
                    if (1 === preg_match('/@LogLevel/', $annotations)) {
                        $value = array_search($value, LogEntityInterface::LOG_LEVEL);
                    }
                }

                $finalString = preg_replace('/'.$stringToReplace.'/', $value, $finalString);
            }
        } catch (ReflectionException $e) {
            throw new LogFieldNotExistsException(sprintf('Field %s doesn\'t exists on Log entity.', $field));
        } catch (TypeError $e) {
            throw new LogFieldTypeErrorException($e->getMessage());
        }

        return $finalString;
    }
}