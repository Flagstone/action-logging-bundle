<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  FlagstoneActionLoggingExtension.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/24
 *  ***************************************************************************************************************** */

namespace Flagstone\ActionLoggingBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Exception;

/** *****************************************************************************************************************
 *  Class FlagstoneActionLoggingExtension
 *  -----------------------------------------------------------------------------------------------------------------
 *  Resources loader class
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\ActionLoggingBundle\DependencyInjection
 *  ***************************************************************************************************************** */
class FlagstoneActionLoggingExtension extends Extension
{
    /** **************************************************************************************************************
     *  Load the resources
     *  --------------------------------------------------------------------------------------------------------------
     *  @param array $configs
     *  @param ContainerBuilder $container
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}