<?php

namespace Flagstone\ActionLoggingBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 *  Class LogLevel
 *  @Annotation
 *  @Target("PROPERTY")
 *
 *  @package Farvest\ActionLoggingBundle\Annotation
 */
class LogLevel
{

}