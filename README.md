# Farvest Action Logging Bundle

Action Logging Bundle by Farvest.

 *  This bundle provide tools to log all action (define by the programmer) on a bundle or an App.
 *  It use a log file or a table (fv_action_log by default) in the database.
 *  A user interface is also develop to access easily to the actions log.